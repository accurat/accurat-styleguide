# Accurat Style Guide

---

## Programming Style Guides

### Javascript

Use [accurapp](https://github.com/accurat/accurapp) to generate the web app.

#### Coding Style

The coding style is based on [Standard JS](https://standardjs.com/), with some customizations. Refer to the [accurapp ESLint configuration](https://github.com/accurat/accurapp/blob/master/packages/eslint-config-accurapp/index.js#L58) for the details (or just learn it by coding with a live linter in the editor).

Some general guidelines:

 * Use ESNext features whenever possible to make the code more concise and explicit. Favor class properties, object spread operator, and similar. You could use other `stage-0` features but it's discouraged.
 * Use many `const`, as few `let` as possible, absolutely no `var`.
 * Favor functions over classes.
 * Favor short functions with a good name over variables.
 * Do not use class inheritance, unless it's required by a library.
 * Favor immutable built-in functions against mutable ones. For example: prefer `Array.concat` over `Array.push`.
 * Favor `Array.map` `Array.forEach` `Array.reduce` over `for` loops.
 * If you make a class, export a function that makes an instance. Do not force the user to use `new`.
 * Prefer export inline with definition: `export default function doSomething() { ... }`.

#### React Props

 * Component listeners should have names starting with `on`, like `onClick` or `onMouseOver`, consistent with `<div>` event listeners.
 * Boolean props should have names starting with `is`, as in `isVisible` or `isHovered`.
 * Use destructuring for props in render function: `function Column({ index, color, isHighlighted }) { ... }`.
 * If the props are too many, use destructuring on `props` in the first line inside the function.
 * For Functional Components do not use arrow functions, they have no name! Simply use `function` in this case.
 * For Class Components extend `React.Component`.
 * Use as few hooks as possible (`componentDidMount`).
 * Do not use `shouldComponentUpdate`, leave it to the state manager.
 * Try not to use `setState` and to favor MobX observables.
 * Destructure `this.props` in the first line of the `render()`.

```js
import React from 'react'

export default class SomeComponent extends React.Component {
  render() {
    const { state, isActive } = this.props
    // ...
  }
}
```

#### MobX State definition

 * Export a function that builds the state, do not export the object or class
 * State is an observable object or a class of observable properties
 * Use `observable.shallow` when you don't need to observe properties inside objects or arrays

```js
class State {
  @observable value = 3
  @observable.shallow list = []
}

export default function buildState() {
  return new State()
}
```

#### MobX Computed Values

 * Use them a lot!

#### MobX Actions

 * Use `@action`s to edit state, don't do it directly by setting the property (unless prototyping).
 * Prefer `@action.bound`, to call the action directly from the component event listener (useful when it accepts no parameters).

```js
class State {
  @action changeSomething(newValue) {
    this.value = newValue
  }

  @action.bound toggleSomething() {
    this.toggle = !this.toggle
  }
}

// Then in a component:

<div onClick={state.toggleSomething}>
  TOGGLE
</div>
```

### HTML

Google style guide HTML/CSS at [http://google.github.io/styleguide/htmlcssguide.html](http://google.github.io/styleguide/htmlcssguide.html)

### CSS

#### Functional CSS

 * Use [Tachyons](http://tachyons.io/) when possible (hint: the fastest guide is the [compiled source code](https://cdnjs.com/libraries/tachyons))
 * Add missing classes in `src/style.css` Tachyons-style (very few rules for every class, possibly one), making the rules non-overlapping ("Functional" or "Immutable" CSS)

#### CSS Modules

 * To write CSS normally, but have every rule namespaced to the component: https://github.com/css-modules/css-modules
 * You probably need to add configuration to webpack's `css-loader`: https://github.com/webpack-contrib/css-loader#modules
 * Use one CSS file per component.
 * Refer to [BEM](http://getbem.com/introduction/) for naming of classes.

---

## Project Structure Style Guide

**Always** make sure everyone can run the project with a simple `yarn start`, and build it with `yarn build`.

For any dependency you can use for example [`concurrently`](https://github.com/kimmobrunfeldt/concurrently) to start multiple processes (a Docker container usually).

### README.md

Always add a `README.md` file on every project to describe what the project is about, how it works and which main technologies it uses.

Important chapters:

 * How to start the project for development (usually `yarn start`).
 * How to build for production (usually `yarn build`).
 * List every other project-specific command.

### Editor Configuration

Use EditorConfig to keep the Editor configuration settings the same on all the projects.
Use the `.editorconfig` file available on this repository and check out [http://editorconfig.org/](http://editorconfig.org).

The **important thing** is to have these settings:

 * Two spaces indentation.
 * No spaces at each end-of-line, Single newline at end-of-file
   + Atom: under the `whitespace` core package: 
     - ✅ Ensure Single Trailing Newline
     - 🚫 Ignore Whitespace On Current Line
     - ✅ Remove Trailing Whitespace
 * Around 100 characters per line, not strictly.

The reason is to avoid code conflicts in Git because of useless spaces.

---

## Versioning Style Guide

### Repository

Create a repository using **lowercase, dash-separated** names, following the template: `customername-projectname`
e.g. `accurat-website`, `bkfs-rap`, `unicredit-expo-webgl`...

Do not use uppercase or underscores.

### Gitignore

Always use the `.gitignore` file available on this repository

### Git

 * Open a feature branch for every feature using **lowercase, dash-separated, < 20 chars** branch names, using feature-centric concise names. (Do not use uppercase or underscores)
 * Use atomic, self-contained, always-working, never-erroring commits if possible. Please, I know it's difficult but try :)
 * For commit messages, start with a verb then describe what you have reached through that commit, not the necessary code changes.
 * Rebase on `origin/master` whenever possible.
 * Push your branch ASAP, even if it's not ready for merge. Every day for example.
 * Make a PR into `master` when you are finished. Describe the feature in the PR text, and make sure everyone can understand what you are talking about. Including a Screenshot / GIF is a good idea.
 * It's **your** responsability to get your branch merged. Don't lose it in the mess. Insist if you see no one cares about it!
 * When you pull, do a `git pull --rebase` (or enable the config option to always do so), to avoid useless merge commits.

---

## Issue Tracking Style Guide

Use BitBucket Issue Tracking to track bugs and issues.

Use Trello for tasks and features, or other equivalent method.